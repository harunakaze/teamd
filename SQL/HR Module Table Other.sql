## SQL BEGIN ##
DROP TABLE IF EXISTS ABSENSI;
DROP TABLE IF EXISTS STAF_PEMBANTU;
DROP TABLE IF EXISTS STAF_DOSEN;
DROP TABLE IF EXISTS STAF;
DROP TABLE IF EXISTS PRODI;
DROP TABLE IF EXISTS FAKULTAS;
DROP TABLE IF EXISTS UNIVERSITAS;
DROP TABLE IF EXISTS JABATAN;

CREATE TABLE JABATAN (
  kode_jabatan INTEGER AUTO_INCREMENT NOT NULL,
  parent_kode_jabatan INTEGER,
  nama_jabatan VARCHAR(35) NOT NULL,
  IS_TETAP BOOLEAN DEFAULT TRUE,
  IS_DELETED BOOLEAN DEFAULT FALSE,
  PRIMARY KEY (kode_jabatan),
  FOREIGN KEY (parent_kode_jabatan) REFERENCES JABATAN(kode_jabatan) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE UNIVERSITAS (
  id_universitas INTEGER AUTO_INCREMENT NOT NULL,
  nama_universitas VARCHAR(125) NOT NULL,
  kepala_universitas INTEGER NOT NULL,
  PRIMARY KEY (id_universitas),
  FOREIGN KEY (kepala_universitas) REFERENCES JABATAN(kode_jabatan) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE FAKULTAS (
  id_fakultas INTEGER AUTO_INCREMENT NOT NULL,
  id_universitas INTEGER NOT NULL,
  nama_fakultas VARCHAR(125) NOT NULL,
  kepala_fakultas INTEGER NOT NULL,
  PRIMARY KEY (id_fakultas),
  FOREIGN KEY (id_universitas) REFERENCES UNIVERSITAS(id_universitas) ON UPDATE CASCADE ON DELETE RESTRICT,  
  FOREIGN KEY (kepala_fakultas) REFERENCES JABATAN(kode_jabatan) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE PRODI (
  id_prodi INTEGER AUTO_INCREMENT NOT NULL,
  id_fakultas INTEGER NOT NULL,
  nama_prodi VARCHAR(125) NOT NULL,
  kepala_prodi INTEGER NOT NULL,
  PRIMARY KEY (id_prodi),
  FOREIGN KEY (id_fakultas) REFERENCES FAKULTAS(id_fakultas) ON UPDATE CASCADE ON DELETE RESTRICT,
  FOREIGN KEY (kepala_prodi) REFERENCES JABATAN(kode_jabatan) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE STAF (
  nip VARCHAR(50) NOT NULL,
  id_universitas INTEGER NOT NULL,
  gaji_pokok NUMERIC NOT NULL,
  nama VARCHAR(255) NOT NULL,
  tempat_lahir VARCHAR(255) NOT NULL,
  tgl_lahir DATE NOT NULL,
  jenis_kelamin CHAR(1) NOT NULL,
  agama VARCHAR(25) NOT NULL,
  alamat VARCHAR(255) NOT NULL,
  tgl_perekrutan DATE NOT NULL,
  IS_DELETED BOOLEAN DEFAULT FALSE,
  PRIMARY KEY (nip, id_universitas),
  FOREIGN KEY (id_universitas) REFERENCES UNIVERSITAS(id_universitas) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE STAF_PEMBANTU (
  nip VARCHAR(50) NOT NULL,
  id_universitas INTEGER NOT NULL,
  kode_jabatan INTEGER NOT NULL,
  PRIMARY KEY (nip, id_universitas),
  FOREIGN KEY (nip, id_universitas) REFERENCES STAF(nip, id_universitas) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (kode_jabatan) REFERENCES JABATAN(kode_jabatan) ON UPDATE CASCADE ON DELETE RESTRICT,
  UNIQUE (kode_jabatan)
);

CREATE TABLE STAF_DOSEN (
  nip VARCHAR(50) NOT NULL,
  id_universitas INTEGER NOT NULL,  
  id_prodi INTEGER NOT NULL,
  nidn VARCHAR(50) NOT NULL,
  pendidikan_terakhir VARCHAR(5) NOT NULL,
  PRIMARY KEY (nip, id_universitas),
  FOREIGN KEY (nip, id_universitas) REFERENCES STAF(nip, id_universitas) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (id_prodi) REFERENCES PRODI(id_prodi) ON UPDATE CASCADE ON DELETE RESTRICT,
  UNIQUE (nidn)
);

CREATE TABLE ABSENSI (
  nip VARCHAR(50) NOT NULL,
  id_universitas INTEGER NOT NULL,
  waktu_absensi TIMESTAMP NOT NULL,
  PRIMARY KEY (nip, id_universitas, waktu_absensi),
  FOREIGN KEY (nip, id_universitas) REFERENCES STAF(nip, id_universitas) ON UPDATE CASCADE ON DELETE RESTRICT
);


## DUMMY DATA ##
INSERT INTO JABATAN (kode_jabatan, parent_kode_jabatan, nama_jabatan) VALUES
(1, NULL, 'Rektor UI'), 
(2, 1, 'Ketua FASILKOM UI'), 
(3, 2, 'Ketua SI UI'),
(4, NULL, 'Rektor Musamus'),
(5, 1, 'Ketua Fakultas 2'),
(6, 2, 'Ketua Prodi 2'),
(7, 3, 'Pekerja 1'),
(8, 3, 'Pekerja 2'),
(9, 3, 'Pekerja 3'),
(10, 3, 'Pekerja 4'),
(11, 8, 'Pekerja 2 1'),
(12, 8, 'Pekerja 2 2'),
(13, 6, 'Pekerja Prodi 2 1'),
(14, 5, 'Ketua Prodi 3');

INSERT INTO UNIVERSITAS (id_universitas, nama_universitas, kepala_universitas) VALUES
(1, 'Universitas Indonesia', 1),
(2, 'Universitas Musamus', 4);

INSERT INTO FAKULTAS (id_fakultas, id_universitas, nama_fakultas, kepala_fakultas) VALUES
(1, 1, 'Ilmu Komputer', 2),
(2, 1, 'Fakultas 2', 5),
(3, 2, 'Fakultas Musamus 1', 5);

INSERT INTO PRODI (id_prodi, id_fakultas, nama_prodi, kepala_prodi) VALUES
(1, 1, 'Sistem Informasi', 3),
(2, 1, 'Prodi 2', 6),
(3, 1, 'Prodi 3', 14),
(4, 3, 'Prodi Musamus 1', 14);

INSERT INTO STAF (nip, id_universitas, gaji_pokok, nama, tempat_lahir, tgl_lahir, jenis_kelamin, agama, alamat, tgl_perekrutan) VALUES
('0001', 1, 15000000, 'Sinus Coaxcika', 'Jakarta', '1975-01-03', 'L', 'Islam', 'Jalan kebon kopi', '2013-05-25'),
('0002', 1, 12000000, 'Alisya Mayer', 'Jember', '1978-05-13', 'P', 'Hindu', 'Jalan kebun jeruk', '2014-01-25'),
('0003', 1, 10000000, 'John Doe', 'Bandung', '1981-02-14', 'L', 'Islam', 'Jalan gunung merapi', '2015-07-15'),
('0004', 1, 9000000, 'Troxaros Defin', 'Austria', '1972-11-25', 'L', 'Buddha', 'Jalan gunung kalem', '2011-09-12'),
('0005', 1, 9000000, 'Alex Keyguard', 'Ireland', '1971-07-23', 'L', 'Islam', 'Jalan tempat tinggal', '2013-03-25'),
('0006', 1, 9000000, 'El Fasicto', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0007', 1, 9000000, 'Orang Fakultas 2', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0008', 1, 9000000, 'Orang Prodi 2', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0009', 1, 9000000, 'Orang Pekerja 1', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0010', 1, 9000000, 'Orang Pekerja 2', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0011', 1, 9000000, 'Orang Pekerja 3', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0012', 1, 9000000, 'Orang Pekerja 4', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0013', 1, 9000000, 'Orang Pekerja 2 1', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0014', 1, 9000000, 'Orang Pekerja 2 1', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0015', 1, 9000000, 'Orang Pekerja Prodi 2 1', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0016', 1, 9000000, 'Orang Prodi 3', 'Spain', '1976-04-17', 'P', 'Kristen', 'Jalan jalan ke taman mini', '2011-09-13'),
('0001', 2, 35000000, 'Forbagos Tredopilio', 'Quebec', '1970-03-23', 'L', 'Islam', 'Jalan jayawijaya', '2009-04-13'),
('0002', 2, 25000000, 'Oshita Bridge', 'China', '1971-03-23', 'P', 'Hindu', 'Jalan puncak raya', '2011-10-25');

INSERT INTO STAF_PEMBANTU (nip, id_universitas, kode_jabatan) VALUES
('0001', 1, 1),
('0002', 1, 2),
('0003', 1, 3),
('0007', 1, 5),
('0008', 1, 6),
('0009', 1, 7),
('0010', 1, 8),
('0011', 1, 9),
('0012', 1, 10),
('0013', 1, 11),
('0014', 1, 12),
('0015', 1, 13),
('0016', 1, 14),
('0001', 2, 4);

INSERT INTO STAF_DOSEN (nip, id_universitas, id_prodi, nidn, pendidikan_terakhir) VALUES
('0004', 1, 1, '000001', 'S2'),
('0005', 1, 1, '000002', 'S3'),
('0006', 1, 1, '000003', 'PhD'),
('0002', 2, 4, '000004', 'PhD');

INSERT INTO ABSENSI (nip, id_universitas, waktu_absensi) VALUES
('0001', 1, '2017-05-20 17:05:24'),
('0001', 1, '2017-05-21 17:04:14'),
('0004', 1, '2017-04-21 18:04:14'),
('0004', 1, '2017-05-22 17:04:14'),
('0004', 1, '2017-05-23 16:54:14');



## END OF SQL ##