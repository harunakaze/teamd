package id.ac.ui.cs.eaap.kelompokD.dao;

import id.ac.ui.cs.eaap.kelompokD.model.StafModel;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by Get-Up on 5/18/2017.
 */
@Mapper
public interface StafMapper {
    @Insert("INSERT INTO staf (nip, id_universitas, gaji_pokok, nama, tempat_lahir, tgl_lahir, jenis_kelamin, agama, alamat, tgl_perekrutan) VALUES " +
            "(#{nip}, #{id_universitas}, #{gaji_pokok}, #{nama}, #{tempat_lahir}, #{tgl_lahir}, #{jenis_kelamin}, #{agama}, #{alamat}, #{tgl_perekrutan})")
    int newStaf(StafModel data);

    @Update("UPDATE staf SET " +
            "gaji_pokok = #{gaji_pokok}, " +
            "nama = #{nama}, " +
            "tempat_lahir = #{tempat_lahir}, " +
            "tgl_lahir = #{tgl_lahir}, " +
            "jenis_kelamin = #{jenis_kelamin}, " +
            "agama = #{agama}, " +
            "alamat = #{alamat}, " +
            "tgl_perekrutan = #{tgl_perekrutan} " +
            "WHERE nip = #{nip} " +
            "AND id_universitas = #{id_universitas}")
    int updStaf(StafModel data);

    @Update("UPDATE staf SET IS_DELETED = TRUE WHERE nip = #{nip} AND id_universitas = #{id_universitas}")
    int delStaf(@Param("nip") String nip, @Param("id_universitas") Integer id_universitas);

    @Select("SELECT * FROM staf WHERE IS_DELETED = FALSE AND nip = #{nip} AND id_universitas = #{id_universitas}")
    StafModel getStaf(@Param("nip") String nip, @Param("id_universitas") Integer id_universitas);

    @Select("SELECT * FROM staf WHERE IS_DELETED = FALSE AND id_universitas = #{id_universitas}")
    List<StafModel> getAllStaf(Integer id_universitas);


}
