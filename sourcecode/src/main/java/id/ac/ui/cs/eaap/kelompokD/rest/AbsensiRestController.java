package id.ac.ui.cs.eaap.kelompokD.rest;

import id.ac.ui.cs.eaap.kelompokD.model.AbsensiModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;
import id.ac.ui.cs.eaap.kelompokD.model.WaktuAbsensiModel;
import id.ac.ui.cs.eaap.kelompokD.service.AbsensiService;
import id.ac.ui.cs.eaap.kelompokD.service.StafPembantuService;
import id.ac.ui.cs.eaap.kelompokD.util.CustomErrorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.YearMonth;
import java.util.List;

/**
 * Created by harunakaze on 28-May-17.
 */
@RestController
public class AbsensiRestController {

    @Autowired
    AbsensiService absensiService;

    @Autowired
    StafPembantuService stafPembantuService;

    @RequestMapping(value = "/rest/{id_universitas}/staf/absensi/{nip}/{year}/{month}", method = RequestMethod.GET)
    public ResponseEntity<?> viewAbsensi(@PathVariable Integer id_universitas, @PathVariable String nip,
                                             @PathVariable Integer year, @PathVariable Integer month) {
        YearMonth yearMonth = getYearMonth(year, month);
        if(yearMonth == null)
            return new ResponseEntity<>(new CustomErrorType("Terdapat kesalahan pada tanggal."),
                HttpStatus.NOT_FOUND);

        StafPembantuModel staf = stafPembantuService.getPembantu(nip, id_universitas);

        if(staf == null) {
            return new ResponseEntity<>(new CustomErrorType("Staff tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        AbsensiModel result = absensiService.getAbsensi(staf, yearMonth);

        if(result == null) {
            return new ResponseEntity<>(new CustomErrorType("Absensi tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/{id_universitas}/universitas/absensi/{year}/{month}", method = RequestMethod.GET)
    public ResponseEntity<?> viewAbsensiUniversitas(@PathVariable Integer id_universitas,
                                             @PathVariable Integer year, @PathVariable Integer month) {
        YearMonth yearMonth = getYearMonth(year, month);
        if(yearMonth == null)
            return new ResponseEntity<>(new CustomErrorType("Terdapat kesalahan pada tanggal."),
                    HttpStatus.NOT_FOUND);

        List<AbsensiModel> result = absensiService.getAbsensiInUniversitas(id_universitas, yearMonth);

        if(result.isEmpty()) {
            return new ResponseEntity<>(new CustomErrorType("Daftar absensi universitas kosong."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/{id_universitas}/staf/absensi/{nip}", method = RequestMethod.POST)
    public ResponseEntity<?> addAbsensi(@PathVariable Integer id_universitas, @PathVariable String nip,
                                        @RequestBody WaktuAbsensiModel waktuAbsen) {
        StafPembantuModel staf = stafPembantuService.getPembantu(nip, id_universitas);

        if(staf == null) {
            return new ResponseEntity<>(new CustomErrorType("Staff tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        if(!absensiService.newAbsensi(staf, waktuAbsen.getWaktu_absensi())) {
            return new ResponseEntity<>(new CustomErrorType("Absensi gagal dimasukkan."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/rest/{id_universitas}/staf/absensi/{nip}", method = RequestMethod.PUT)
    public ResponseEntity<?> updAbsensi(@PathVariable Integer id_universitas, @PathVariable String nip,
                                        @RequestBody WaktuAbsensiModel waktuAbsen) {
        StafPembantuModel staf = stafPembantuService.getPembantu(nip, id_universitas);

        if(staf == null) {
            return new ResponseEntity<>(new CustomErrorType("Staff tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        if(!absensiService.updAbsensi(staf, waktuAbsen.getWaktu_absensi(), waktuAbsen.getNew_waktu_absensi())) {
            return new ResponseEntity<>(new CustomErrorType("Absensi gagal diperbaharui."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/{id_universitas}/staf/absensi/{nip}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteAbsensi(@PathVariable Integer id_universitas, @PathVariable String nip,
                                        @RequestBody WaktuAbsensiModel waktuAbsen) {

        StafPembantuModel staf = stafPembantuService.getPembantu(nip, id_universitas);

        if(staf == null) {
            return new ResponseEntity<>(new CustomErrorType("Staff tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        if(!absensiService.delAbsensi(staf, waktuAbsen.getWaktu_absensi())) {
            return new ResponseEntity<>(new CustomErrorType("Absensi gagal dihapus."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    private YearMonth getYearMonth(Integer year, Integer month) {
        YearMonth yearMonth;

        try {
            yearMonth = YearMonth.of(year, month);
        } catch (Exception e) {
            return null;
        }

        return yearMonth;
    }
}
