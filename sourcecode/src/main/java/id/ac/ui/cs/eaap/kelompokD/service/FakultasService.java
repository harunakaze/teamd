package id.ac.ui.cs.eaap.kelompokD.service;

import id.ac.ui.cs.eaap.kelompokD.model.FakultasModel;
import id.ac.ui.cs.eaap.kelompokD.model.JabatanModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;

import java.util.List;

/**
 * Created by harunakaze on 26-May-17.
 */
public interface FakultasService {

    boolean newFakultas(FakultasModel newFakultas);

    boolean updFakultas(FakultasModel fakultas);

    FakultasModel getFakultas(Integer id_fakultas);

    List<FakultasModel> getAllFakultas(Integer id_universitas);

    boolean isFakultasExist(FakultasModel fakultas);

    JabatanModel getKetuaFakultas(Integer id_fakultas);
    List<StafPembantuModel> getAllStafPembantu(Integer id_fakultas);
}
