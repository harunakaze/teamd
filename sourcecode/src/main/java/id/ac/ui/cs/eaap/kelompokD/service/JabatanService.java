package id.ac.ui.cs.eaap.kelompokD.service;

import id.ac.ui.cs.eaap.kelompokD.model.JabatanModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Get-Up on 5/18/2017.
 */
public interface JabatanService {
    boolean newJabatan(JabatanModel data);

    boolean updJabatan(JabatanModel data);

    boolean delJabatan(Integer kode_jabatan);

    boolean isJabatanExist(JabatanModel jabatan);

    List<JabatanModel> getAllJabatan(Integer id_universitas);

    JabatanModel getJabatan(Integer kode_jabatan);

    JabatanModel getStructureFromRoot(Integer kode_jabatan);
}
