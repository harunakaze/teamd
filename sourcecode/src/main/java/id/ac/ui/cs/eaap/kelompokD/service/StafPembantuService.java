package id.ac.ui.cs.eaap.kelompokD.service;

import java.util.List;

import id.ac.ui.cs.eaap.kelompokD.model.JabatanModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;

public interface StafPembantuService {

	boolean newPembantu(StafPembantuModel newPembantu);

    boolean updPembantu(StafPembantuModel pembantu);

    boolean isPembantuExist(StafPembantuModel pembantu);

	List<StafPembantuModel> getAllPembantu(Integer id_universitas);
	StafPembantuModel getPembantu(String nip, Integer id_universitas);

	StafPembantuModel getPembantuFromJabatan(Integer kode_jabatan);
	void getStafData(JabatanModel jabatan, List<StafPembantuModel> listStaf);
}
