package id.ac.ui.cs.eaap.kelompokD.service;

import id.ac.ui.cs.eaap.kelompokD.dao.AbsensiMapper;
import id.ac.ui.cs.eaap.kelompokD.model.AbsensiModel;
import id.ac.ui.cs.eaap.kelompokD.model.KehadiranDoseModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Year;
import java.time.YearMonth;
import java.util.Date;
import java.util.List;

/**
 * Created by harunakaze on 27-May-17.
 */
@Service
public class AbsensiServiceImplementation implements AbsensiService {

    @Autowired
    AbsensiMapper absensiDAO;

    @Autowired
    RestTemplate restTemplate;

    @Override
    public boolean newAbsensi(StafModel stafModel, Date waktu_absensi) {
        return (absensiDAO.newAbsensi(stafModel, waktu_absensi) != 0);
    }

    @Override
    public boolean updAbsensi(StafModel stafModel, Date waktu_absensi, Date new_waktu_absensi) {
        return (absensiDAO.updAbsensi(stafModel, waktu_absensi, new_waktu_absensi) != 0);
    }

    @Override
    public boolean delAbsensi(StafModel stafModel, Date waktu_absensi) {
        return (absensiDAO.delAbsensi(stafModel, waktu_absensi) != 0);
    }

    @Override
    public List<AbsensiModel> getAbsensiInUniversitas(Integer id_universitas, YearMonth yearMonth) {
        Integer month = yearMonth.getMonthValue();
        Integer year = yearMonth.getYear();
        return absensiDAO.getAbsensiInUniversitas(id_universitas, month, year);
    }

    @Override
    public AbsensiModel getAbsensi(StafModel stafModel, YearMonth yearMonth) {
        Integer month = yearMonth.getMonthValue();
        Integer year = yearMonth.getYear();
        return absensiDAO.getAbsensi(stafModel, month, year);
    }

    @Override
    public Integer getJumlahKehadiranDosen(String nidn, YearMonth yearMonth) {
        Integer month = yearMonth.getMonthValue();
        Integer year = yearMonth.getYear();
        String url = String.format("http://localhost:8080/rest/dosen/kehadiran/%s/%d/%d", nidn, year, month);
        KehadiranDoseModel kehadiran = restTemplate.getForObject(url, KehadiranDoseModel.class);

        return kehadiran.getJumlah_hadir();
    }
}
