package id.ac.ui.cs.eaap.kelompokD.dictionary;

/**
 * Created by Get-Up on 5/18/2017.
 */
public class HR_DICTIONARY {
    public static String REST_URL = "/rest";
    public static String INSERT_JABATAN = "/jabatan/new";
    public static String UPDATE_JABATAN = "/jabatan/upd";
    public static String DELETE_JABATAN = "/jabatan/del";
    public static String VIEW_ALL_JABATAN = "/jabatan/viewall";
}
