package id.ac.ui.cs.eaap.kelompokD.rest;

import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;
import id.ac.ui.cs.eaap.kelompokD.service.StafPembantuService;
import id.ac.ui.cs.eaap.kelompokD.util.CustomErrorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by harunakaze on 28-May-17.
 */
@RestController
public class PembantuRestController {

    @Autowired
    StafPembantuService stafPembantuService;

    @RequestMapping(value = "/rest/{id_universitas}/pembantu", method = RequestMethod.GET)
    public ResponseEntity<?> viewAllPembantu(@PathVariable Integer id_universitas) {
        List<StafPembantuModel> pembantu = stafPembantuService.getAllPembantu(id_universitas);

        if(pembantu.isEmpty()) {
            return new ResponseEntity<>(new CustomErrorType("Daftar staf pembantu kosong."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(pembantu, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/{id_universitas}/pembantu/{nip}", method = RequestMethod.GET)
    public ResponseEntity<?> viewPembantu(@PathVariable String nip, @PathVariable Integer id_universitas) {
        StafPembantuModel result = stafPembantuService.getPembantu(nip, id_universitas);

        if(result == null) {
            return new ResponseEntity<>(new CustomErrorType("Staf pembantu tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/{id_universitas}/pembantu", method = RequestMethod.POST)
    public ResponseEntity<?> addPembantu(@PathVariable Integer id_universitas,
                                         @RequestBody StafPembantuModel newPembantu, UriComponentsBuilder builder) {
        // Set user model id
        newPembantu.setId_universitas(id_universitas);

        if(stafPembantuService.isPembantuExist(newPembantu)) {
            return new ResponseEntity<>(new CustomErrorType("Staf pembantu sudah ada."),
                    HttpStatus.CONFLICT);
        }

        if(!stafPembantuService.newPembantu(newPembantu)) {
            return new ResponseEntity<>(new CustomErrorType("Staf pembantu gagal dimasukkan."),
                    HttpStatus.NO_CONTENT);
        }

        UriComponents locationHeader =
                builder.path("/rest/{id_universitas}/pembantu/{nip}").buildAndExpand(
                        newPembantu.getId_universitas(),
                        newPembantu.getNip());
        return ResponseEntity.created(locationHeader.toUri()).build();
    }

    @RequestMapping(value = "/rest/{id_universitas}/pembantu/{nip}", method = RequestMethod.PUT)
    public ResponseEntity<?> updUniversitas(@PathVariable Integer id_universitas, @PathVariable String nip,
                                            @RequestBody StafPembantuModel pembantu) {
        // Set user model id
        pembantu.setId_universitas(id_universitas);
        pembantu.setNip(nip);

        StafPembantuModel findModel = stafPembantuService.getPembantu(pembantu.getNip(), pembantu.getId_universitas());

        if(findModel == null) {
            return new ResponseEntity<>(new CustomErrorType("Staf pembantu tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        if(!stafPembantuService.updPembantu(pembantu)) {
            return new ResponseEntity<>(new CustomErrorType("Staf pembantu gagal diperbaharui."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(pembantu, HttpStatus.OK);
    }
}
