package id.ac.ui.cs.eaap.kelompokD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class HrModuleTeamDApplication {

	public static void main(String[] args) {
		SpringApplication.run(HrModuleTeamDApplication.class, args);
	}

	// Need this for REST request?
    // https://docs.spring.io/spring-boot/docs/1.4.0.RELEASE/reference/html/boot-features-restclient.html
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
