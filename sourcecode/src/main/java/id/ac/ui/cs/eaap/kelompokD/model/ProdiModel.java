package id.ac.ui.cs.eaap.kelompokD.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by harunakaze on 25-May-17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProdiModel {
    private Integer id_prodi;
    private Integer id_fakultas;
    private String nama_prodi;
    private Integer kepala_prodi;
}
