package id.ac.ui.cs.eaap.kelompokD.dao;

import id.ac.ui.cs.eaap.kelompokD.model.AbsensiModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafModel;
import org.apache.ibatis.annotations.*;

import java.time.YearMonth;
import java.util.Date;
import java.util.List;

/**
 * Created by harunakaze on 27-May-17.
 */
@Mapper
public interface AbsensiMapper {

    @Insert("INSERT INTO ABSENSI (nip, id_universitas, waktu_absensi) VALUES " +
            "(#{stafModel.nip}, #{stafModel.id_universitas}, #{waktu_absensi})")
    int newAbsensi(@Param("stafModel") StafModel stafModel,
                   @Param("waktu_absensi") Date waktu_absensi);

    @Delete("DELETE FROM absensi " +
            "WHERE nip = #{stafModel.nip} AND id_universitas = #{stafModel.id_universitas} AND waktu_absensi = #{waktu_absensi}")
    int delAbsensi(@Param("stafModel") StafModel stafModel,
                   @Param("waktu_absensi") Date waktu_absensi);

    @Update("UPDATE absensi SET waktu_absensi = #{new_waktu_absensi} " +
            "WHERE nip = #{stafModel.nip} AND id_universitas = #{stafModel.id_universitas} AND waktu_absensi = #{waktu_absensi}")
    int updAbsensi(@Param("stafModel") StafModel stafModel,
                   @Param("waktu_absensi") Date waktu_absensi, @Param("new_waktu_absensi") Date new_waktu_absensi);

    @Select("SELECT *, #{month} as month, #{year} as year " +
            "FROM staf " +
            "WHERE IS_DELETED = FALSE AND id_universitas = #{id_universitas}")
    @Results(value = {
            @Result(property = "waktu_absen",
                    column = "{nip = nip, id_universitas = id_universitas, " +
                            "month = month, year = year}",
                    javaType = List.class,
                    many = @Many(select = "getWaktuAbsensi")),
            @Result(property = "jumlah_hadir",
                    column = "{nip = nip, id_universitas = id_universitas, " +
                            "month = month, year = year}",
                    javaType = Integer.class,
                    one = @One(select = "getJumlahHadir"))
    })
    List<AbsensiModel> getAbsensiInUniversitas(@Param("id_universitas") Integer id_universitas,
                                         @Param("month") Integer month, @Param("year") Integer year);

    @Select("SELECT *, #{month} as month, #{year} as year " +
            "FROM staf " +
            "WHERE IS_DELETED = FALSE AND nip = #{stafModel.nip} AND id_universitas = #{stafModel.id_universitas}")
    @Results(value = {
            @Result(property = "waktu_absen",
                    column = "{nip = nip, id_universitas = id_universitas, " +
                             "month = month, year = year}",
                    javaType = List.class,
                    many = @Many(select = "getWaktuAbsensi")),
            @Result(property = "jumlah_hadir",
                    column = "{nip = nip, id_universitas = id_universitas, " +
                             "month = month, year = year}",
                    javaType = Integer.class,
                    one = @One(select = "getJumlahHadir"))
    })
    AbsensiModel getAbsensi(@Param("stafModel") StafModel stafModel,
                            @Param("month") Integer month, @Param("year") Integer year);

    @Select("SELECT COUNT(*) FROM absensi WHERE nip = #{nip} AND id_universitas = #{id_universitas} " +
            "AND YEAR(waktu_absensi) = #{year} AND MONTH(waktu_absensi) = #{month}")
    Integer getJumlahHadir(@Param("nip") String nip, @Param("id_universitas") Integer id_universitas,
                           @Param("month") Integer month, @Param("year") Integer year);

    @Select("SELECT waktu_absensi FROM absensi WHERE nip = #{nip} AND id_universitas = #{id_universitas} " +
            "AND YEAR(waktu_absensi) = #{year} AND MONTH(waktu_absensi) = #{month}")
    List<Date> getWaktuAbsensi(@Param("nip") String nip, @Param("id_universitas") Integer id_universitas,
                                    @Param("month") Integer month, @Param("year") Integer year);
}
