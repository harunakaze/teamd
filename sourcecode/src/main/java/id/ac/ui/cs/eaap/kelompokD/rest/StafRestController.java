package id.ac.ui.cs.eaap.kelompokD.rest;

import id.ac.ui.cs.eaap.kelompokD.model.StafModel;
import id.ac.ui.cs.eaap.kelompokD.service.StafService;
import id.ac.ui.cs.eaap.kelompokD.util.CustomErrorType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by Get-Up on 5/18/2017.
 */
@Slf4j
@RestController
public class StafRestController {

    @Autowired
    StafService stafService;

    @RequestMapping(value = "/rest/{id_universitas}/staf", method = RequestMethod.GET)
    public ResponseEntity<?> viewAll(@PathVariable Integer id_universitas){
        List<StafModel> staffs = stafService.getAllStaf(id_universitas);

        if(staffs.isEmpty()) {
            return new ResponseEntity<>(new CustomErrorType("Daftar staf kosong."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(staffs, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/{id_universitas}/staf/{nip}", method = RequestMethod.GET)
    public ResponseEntity<?> getStaff(@PathVariable Integer id_universitas, @PathVariable String nip) {
        StafModel result = stafService.getStaf(nip, id_universitas);

        if(result == null) {
            return new ResponseEntity<>(new CustomErrorType("Staf tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/{id_universitas}/staf", method = RequestMethod.POST)
    public ResponseEntity<?> addStaf(@PathVariable Integer id_universitas,
                                            @RequestBody StafModel newStaf, UriComponentsBuilder builder) {
        // Set user model id
        newStaf.setId_universitas(id_universitas);

        if(stafService.isStafExist(newStaf)) {
            return new ResponseEntity<>(new CustomErrorType("Staf sudah ada."),
                    HttpStatus.CONFLICT);
        }

        if(!stafService.newStaf(newStaf)) {
            return new ResponseEntity<>(new CustomErrorType("Staf gagal dimasukkan."),
                    HttpStatus.NO_CONTENT);
        }

        UriComponents locationHeader =
                builder.path("/rest/{id_universitas}/staf/{nip}").buildAndExpand(
                        newStaf.getId_universitas(), newStaf.getNip());
        return ResponseEntity.created(locationHeader.toUri()).build();
    }

    @RequestMapping(value = "/rest/{id_universitas}/staf/{nip}", method = RequestMethod.PUT)
    public ResponseEntity<?> updStaff(@PathVariable Integer id_universitas, @PathVariable String nip,
                                      @RequestBody StafModel staf) {
        // Set user model id
        staf.setId_universitas(id_universitas);
        staf.setNip(nip);


        StafModel findModel = stafService.getStaf(staf.getNip(), staf.getId_universitas());

        if(findModel == null) {
            return new ResponseEntity<>(new CustomErrorType("Staf tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        if(!stafService.updStaf(staf)) {
            return new ResponseEntity<>(new CustomErrorType("Staf gagal diperbaharui."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(staf, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/{id_universitas}/staf/{nip}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteStaf(@PathVariable Integer id_universitas, @PathVariable String nip) {

        StafModel findModel = stafService.getStaf(nip, id_universitas);

        if(findModel == null) {
            return new ResponseEntity<>(new CustomErrorType("Staf tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        if(!stafService.delStaf(nip, id_universitas)) {
            return new ResponseEntity<>(new CustomErrorType("Staf gagal dihapus."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
