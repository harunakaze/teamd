package id.ac.ui.cs.eaap.kelompokD.rest;

import id.ac.ui.cs.eaap.kelompokD.model.StafDosenModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;
import id.ac.ui.cs.eaap.kelompokD.service.StafDosenService;
import id.ac.ui.cs.eaap.kelompokD.util.CustomErrorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by harunakaze on 26-May-17.
 */
@RestController
public class DosenRestController {

    @Autowired
    StafDosenService stafDosenService;

    @RequestMapping(value = "/rest/dosen/{nidn}", method = RequestMethod.GET)
    public ResponseEntity<?> getDosen(@PathVariable String nidn) {
        StafDosenModel result = stafDosenService.getDosen(nidn);

        if(result == null) {
            return new ResponseEntity<>(new CustomErrorType("Dosen tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/{id_universitas}/dosen/viewall", method = RequestMethod.GET)
    public ResponseEntity<?> getDosenAll(@PathVariable Integer id_universitas) {
        List<StafDosenModel> dosens = stafDosenService.getAllDosen(id_universitas);

        if(dosens.isEmpty()) {
            return new ResponseEntity<>(new CustomErrorType("Daftar dosen kosong."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(dosens, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/{id_universitas}/dosen/prodi/viewall/{id_prodi}", method = RequestMethod.GET)
    public ResponseEntity<?> getDosenAllInProdi(@PathVariable Integer id_prodi) {
        List<StafDosenModel> dosens = stafDosenService.getAllDosenInProdi(id_prodi);

        if(dosens.isEmpty()) {
            return new ResponseEntity<>(new CustomErrorType("Daftar dosen kosong."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(dosens, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/dosen", method = RequestMethod.POST)
    public ResponseEntity<?> addDosen(@RequestBody StafDosenModel newDosen, UriComponentsBuilder builder) {

        if(stafDosenService.isDosenExist(newDosen)) {
            return new ResponseEntity<>(new CustomErrorType("Dosen sudah ada."),
                    HttpStatus.CONFLICT);
        }

        if(!stafDosenService.newDosen(newDosen)) {
            return new ResponseEntity<>(new CustomErrorType("Dosen gagal dimasukkan."),
                    HttpStatus.NO_CONTENT);
        }

        UriComponents locationHeader =
                builder.path("/rest/dosen/{nidn}").buildAndExpand(newDosen.getNidn());
        return ResponseEntity.created(locationHeader.toUri()).build();
    }

    @RequestMapping(value = "/rest/dosen/{nidn}", method = RequestMethod.PUT)
    public ResponseEntity<?> updUniversitas(@PathVariable String nidn, @RequestBody StafDosenModel dosen) {
        // Set user model id
        dosen.setNidn(nidn);

        StafDosenModel findModel = stafDosenService.getDosen(nidn);

        if(findModel == null) {
            return new ResponseEntity<>(new CustomErrorType("Dosen tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        if(!stafDosenService.updDosen(dosen)) {
            return new ResponseEntity<>(new CustomErrorType("Dosen gagal diperbaharui."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(dosen, HttpStatus.OK);
    }
}
