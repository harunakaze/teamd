package id.ac.ui.cs.eaap.kelompokD.rest;

import id.ac.ui.cs.eaap.kelompokD.model.JabatanModel;
import id.ac.ui.cs.eaap.kelompokD.service.FakultasService;
import id.ac.ui.cs.eaap.kelompokD.service.JabatanService;
import id.ac.ui.cs.eaap.kelompokD.service.ProdiService;
import id.ac.ui.cs.eaap.kelompokD.util.CustomErrorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by harunakaze on 25-May-17.
 */
@RestController
public class JabatanRestController {

    @Autowired
    JabatanService jabatanService;

    @Autowired
    FakultasService fakultasService;

    @Autowired
    ProdiService prodiService;

    @RequestMapping(value = "/rest/{id_universitas}/jabatan", method = RequestMethod.GET)
    public ResponseEntity<?> viewAll(@PathVariable Integer id_universitas){
        List<JabatanModel> results = jabatanService.getAllJabatan(id_universitas);

        if(results.isEmpty()) {
            return new ResponseEntity<>(new CustomErrorType("Daftar jabatan kosong."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/jabatan/{kode_jabatan}", method = RequestMethod.GET)
    public ResponseEntity<?> viewJabatan(@PathVariable Integer kode_jabatan) {
        JabatanModel result = jabatanService.getJabatan(kode_jabatan);

        if(result == null) {
            return new ResponseEntity<>(new CustomErrorType("Jabatan tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/jabatan", method = RequestMethod.POST)
    public ResponseEntity<?> addJabatan(@RequestBody JabatanModel newJabatan, UriComponentsBuilder builder) {

        if(jabatanService.isJabatanExist(newJabatan)) {
            return new ResponseEntity<>(new CustomErrorType("Jabatan sudah ada."),
                    HttpStatus.CONFLICT);
        }

        if(!jabatanService.newJabatan(newJabatan)) {
            return new ResponseEntity<>(new CustomErrorType("Jabatan gagal dimasukkan."),
                    HttpStatus.NO_CONTENT);
        }

        UriComponents locationHeader =
                builder.path("/rest/jabatan/{kode_jabatan}").buildAndExpand(newJabatan.getKode_jabatan());
        return ResponseEntity.created(locationHeader.toUri()).build();
    }

    @RequestMapping(value = "/rest/jabatan/{kode_jabatan}", method = RequestMethod.PUT)
    public ResponseEntity<?> updJabatan(@PathVariable Integer kode_jabatan, @RequestBody JabatanModel jabatan) {
        // Set user model id
        jabatan.setKode_jabatan(kode_jabatan);

        JabatanModel findModel = jabatanService.getJabatan(kode_jabatan);

        if(findModel == null) {
            return new ResponseEntity<>(new CustomErrorType("Jabatan tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        if(!jabatanService.updJabatan(jabatan)) {
            return new ResponseEntity<>(new CustomErrorType("Jabatan gagal diperbaharui."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(jabatan, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/jabatan/{kode_jabatan}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteJabatan(@PathVariable Integer kode_jabatan) {

        JabatanModel findModel = jabatanService.getJabatan(kode_jabatan);

        if(findModel == null) {
            return new ResponseEntity<>(new CustomErrorType("Jabatan tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        if(!jabatanService.delJabatan(kode_jabatan)) {
            return new ResponseEntity<>(new CustomErrorType("Jabatan gagal dihapus."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // Structure root request
    @RequestMapping("/rest/{id_universitas}/fakultas/chart/{id_fakultas}")
    public JabatanModel viewFakultas(@PathVariable Integer id_universitas, @PathVariable Integer id_fakultas){
        JabatanModel ketuaFakultas = fakultasService.getKetuaFakultas(id_fakultas);
        Integer kode_jabatan = ketuaFakultas.getKode_jabatan();

        JabatanModel results = jabatanService.getStructureFromRoot(kode_jabatan);

        // So google org chart would render it correctly
        results.setParent_kode_jabatan(null);
        return results;
    }

    @RequestMapping("/rest/{id_universitas}/prodi/chart/{id_prodi}")
    public JabatanModel viewProdi(@PathVariable Integer id_universitas, @PathVariable Integer id_prodi){
        JabatanModel ketuaProdi = prodiService.getKetuaProdi(id_prodi);
        Integer kode_jabatan = ketuaProdi.getKode_jabatan();

        JabatanModel results = jabatanService.getStructureFromRoot(kode_jabatan);

        // So google org chart would render it correctly
        results.setParent_kode_jabatan(null);
        return results;
    }
}
