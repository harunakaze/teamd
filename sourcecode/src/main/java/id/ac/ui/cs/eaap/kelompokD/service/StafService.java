package id.ac.ui.cs.eaap.kelompokD.service;

import id.ac.ui.cs.eaap.kelompokD.model.StafModel;

import java.util.List;

/**
 * Created by Get-Up on 5/18/2017.
 */
public interface StafService {
    boolean newStaf(StafModel data);

    boolean updStaf(StafModel data);

    boolean delStaf(String nip, Integer id_universitas);

    boolean isStafExist(StafModel staf);

    StafModel getStaf(String nip, Integer id_universitas);

    List<StafModel> getAllStaf(Integer id_universitas);
}
