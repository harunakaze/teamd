package id.ac.ui.cs.eaap.kelompokD.rest;

import id.ac.ui.cs.eaap.kelompokD.model.ProdiModel;
import id.ac.ui.cs.eaap.kelompokD.service.ProdiService;
import id.ac.ui.cs.eaap.kelompokD.util.CustomErrorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by harunakaze on 28-May-17.
 */
@RestController
public class ProdiRestController {

    @Autowired
    ProdiService prodiService;

    @RequestMapping(value = "/rest/fakultas/{id_fakultas}/prodi", method = RequestMethod.GET)
    public ResponseEntity<?> viewAllProdi(@PathVariable Integer id_fakultas) {
        List<ProdiModel> prodi = prodiService.getAllProdi(id_fakultas);

        if(prodi.isEmpty()) {
            return new ResponseEntity<>(new CustomErrorType("Daftar prodi kosong."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(prodi, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/prodi/{id_prodi}", method = RequestMethod.GET)
    public ResponseEntity<?> viewProdi(@PathVariable Integer id_prodi) {
        ProdiModel result = prodiService.getProdi(id_prodi);

        if(result == null) {
            return new ResponseEntity<>(new CustomErrorType("Prodi tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/prodi", method = RequestMethod.POST)
    public ResponseEntity<?> addProdi(@RequestBody ProdiModel newProdi, UriComponentsBuilder builder) {

        if(prodiService.isProdiExist(newProdi)) {
            return new ResponseEntity<>(new CustomErrorType("Prodi sudah ada."),
                    HttpStatus.CONFLICT);
        }

        if(!prodiService.newProdi(newProdi)) {
            return new ResponseEntity<>(new CustomErrorType("Prodi gagal dimasukkan."),
                    HttpStatus.NO_CONTENT);
        }

        UriComponents locationHeader =
                builder.path("/rest/prodi/{id_prodi}").buildAndExpand(newProdi.getId_prodi());
        return ResponseEntity.created(locationHeader.toUri()).build();
    }

    @RequestMapping(value = "/rest/prodi/{id_prodi}", method = RequestMethod.PUT)
    public ResponseEntity<?> updProdi(@PathVariable Integer id_prodi, @RequestBody ProdiModel prodi) {
        // Set user model id
        prodi.setId_prodi(id_prodi);

        ProdiModel findModel = prodiService.getProdi(id_prodi);

        if(findModel == null) {
            return new ResponseEntity<>(new CustomErrorType("Prodi tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        if(!prodiService.updProdi(prodi)) {
            return new ResponseEntity<>(new CustomErrorType("Prodi gagal diperbaharui."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(prodi, HttpStatus.OK);
    }
}
