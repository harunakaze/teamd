package id.ac.ui.cs.eaap.kelompokD.service;

import id.ac.ui.cs.eaap.kelompokD.dao.ProdiMapper;
import id.ac.ui.cs.eaap.kelompokD.model.JabatanModel;
import id.ac.ui.cs.eaap.kelompokD.model.ProdiModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harunakaze on 26-May-17.
 */
@Service
public class ProdiServiceImplementation implements ProdiService {

    @Autowired
    ProdiMapper prodiDAO;

    @Autowired
    JabatanService jabatanService;

    @Autowired
    StafPembantuService stafPembantuService;

    @Override
    public boolean newProdi(ProdiModel newProdi) {
        return (prodiDAO.newProdi(newProdi) != 0);
    }

    @Override
    public boolean updProdi(ProdiModel prodi) {
        return (prodiDAO.updProdi(prodi) != 0);
    }

    @Override
    public ProdiModel getProdi(Integer id_prodi) {
        return prodiDAO.getProdi(id_prodi);
    }

    @Override
    public List<ProdiModel> getAllProdi(Integer id_fakultas) {
        return prodiDAO.getAllProdi(id_fakultas);
    }

    @Override
    public boolean isProdiExist(ProdiModel prodi) {
        return (getProdi(prodi.getId_prodi()) != null);
    }

    @Override
    public JabatanModel getKetuaProdi(Integer id_prodi) {
        return prodiDAO.getKetuaProdi(id_prodi);
    }

    @Override
    public List<StafPembantuModel> getAllStafPembantu(Integer id_prodi) {
        JabatanModel ketuaProdi = getKetuaProdi(id_prodi);
        Integer kode_jabatan = ketuaProdi.getKode_jabatan();

        JabatanModel jabatanModel = jabatanService.getStructureFromRoot(kode_jabatan);

        List<StafPembantuModel> listStaf = new ArrayList<>();
        stafPembantuService.getStafData(jabatanModel, listStaf);

        return listStaf;
    }
}
