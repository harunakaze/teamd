package id.ac.ui.cs.eaap.kelompokD.rest;

import id.ac.ui.cs.eaap.kelompokD.model.FakultasModel;
import id.ac.ui.cs.eaap.kelompokD.service.FakultasService;
import id.ac.ui.cs.eaap.kelompokD.util.CustomErrorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by harunakaze on 28-May-17.
 */
@RestController
public class FakultasRestController {

    @Autowired
    FakultasService fakultasService;

    @RequestMapping(value = "/rest/{id_universitas}/fakultas", method = RequestMethod.GET)
    public ResponseEntity<?> viewAllFakultas(@PathVariable Integer id_universitas) {
        List<FakultasModel> fakultas = fakultasService.getAllFakultas(id_universitas);

        if(fakultas.isEmpty()) {
            return new ResponseEntity<>(new CustomErrorType("Daftar fakultas kosong."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(fakultas, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/fakultas/{id_fakultas}", method = RequestMethod.GET)
    public ResponseEntity<?> viewFakultas(@PathVariable Integer id_fakultas) {
        FakultasModel result = fakultasService.getFakultas(id_fakultas);

        if(result == null) {
            return new ResponseEntity<>(new CustomErrorType("Fakultas tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/fakultas", method = RequestMethod.POST)
    public ResponseEntity<?> addFakultas(@RequestBody FakultasModel newFakultas, UriComponentsBuilder builder) {

        if(fakultasService.isFakultasExist(newFakultas)) {
            return new ResponseEntity<>(new CustomErrorType("Fakultas sudah ada."),
                    HttpStatus.CONFLICT);
        }

        if(!fakultasService.newFakultas(newFakultas)) {
            return new ResponseEntity<>(new CustomErrorType("Fakultas gagal dimasukkan."),
                    HttpStatus.NO_CONTENT);
        }

        UriComponents locationHeader =
                builder.path("/rest/fakultas/{id_fakultas}").buildAndExpand(newFakultas.getId_fakultas());
        return ResponseEntity.created(locationHeader.toUri()).build();
    }

    @RequestMapping(value = "/rest/fakultas/{id_fakultas}", method = RequestMethod.PUT)
    public ResponseEntity<?> updFakultas(@PathVariable Integer id_fakultas, @RequestBody FakultasModel fakultas) {
        // Set user model id
        fakultas.setId_fakultas(id_fakultas);

        FakultasModel findModel = fakultasService.getFakultas(id_fakultas);

        if(findModel == null) {
            return new ResponseEntity<>(new CustomErrorType("Fakultas tidak ditemukan."),
                    HttpStatus.NOT_FOUND);
        }

        if(!fakultasService.updFakultas(fakultas)) {
            return new ResponseEntity<>(new CustomErrorType("Fakultas gagal diperbaharui."),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(fakultas, HttpStatus.OK);
    }
}
