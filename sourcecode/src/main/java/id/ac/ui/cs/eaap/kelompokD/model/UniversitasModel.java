package id.ac.ui.cs.eaap.kelompokD.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by harunakaze on 25-May-17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UniversitasModel {
    private Integer id_universitas;
    private String nama_universitas;
    private Integer kepala_universitas;
}
