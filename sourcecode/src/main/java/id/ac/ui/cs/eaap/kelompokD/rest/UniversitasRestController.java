package id.ac.ui.cs.eaap.kelompokD.rest;

import id.ac.ui.cs.eaap.kelompokD.model.UniversitasModel;
import id.ac.ui.cs.eaap.kelompokD.service.UniversitasService;
import id.ac.ui.cs.eaap.kelompokD.util.CustomErrorType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by harunakaze on 27-May-17.
 */
@Slf4j
@RestController
public class UniversitasRestController {

    @Autowired
    UniversitasService universitasService;

    @RequestMapping(value = "/rest/universitas", method = RequestMethod.GET)
    public ResponseEntity<?> viewAllUniversitas() {
        List<UniversitasModel> universitas = universitasService.getAllUniversitas();

        if(universitas.isEmpty()) {
            return new ResponseEntity<>(new CustomErrorType("Daftar universitas kosong."),
                                        HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(universitas, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/universitas/{id_universitas}", method = RequestMethod.GET)
    public ResponseEntity<?> viewUniversitas(@PathVariable Integer id_universitas) {
        UniversitasModel result = universitasService.getUniversitas(id_universitas);

        if(result == null) {
            return new ResponseEntity<>(new CustomErrorType("Universitas tidak ditemukan."),
                                        HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/rest/universitas", method = RequestMethod.POST)
    public ResponseEntity<?> addUniversitas(@RequestBody UniversitasModel newUniv, UriComponentsBuilder builder) {

        if(universitasService.isUniversitasExist(newUniv)) {
            return new ResponseEntity<>(new CustomErrorType("Universitas sudah ada."),
                                        HttpStatus.CONFLICT);
        }

        if(!universitasService.newUniversitas(newUniv)) {
            return new ResponseEntity<>(new CustomErrorType("Universitas gagal dimasukkan."),
                                        HttpStatus.NO_CONTENT);
        }

        UriComponents locationHeader =
                builder.path("/rest/universitas/{id_universitas}").buildAndExpand(newUniv.getId_universitas());
        return ResponseEntity.created(locationHeader.toUri()).build();
    }

    @RequestMapping(value = "/rest/universitas/{id_universitas}", method = RequestMethod.PUT)
    public ResponseEntity<?> updUniversitas(@PathVariable Integer id_universitas, @RequestBody UniversitasModel univ) {
        // Set user model id
        univ.setId_universitas(id_universitas);

        UniversitasModel findModel = universitasService.getUniversitas(id_universitas);

        if(findModel == null) {
            return new ResponseEntity<>(new CustomErrorType("Universitas tidak ditemukan."),
                                        HttpStatus.NOT_FOUND);
        }

        if(!universitasService.updUniversitas(univ)) {
            return new ResponseEntity<>(new CustomErrorType("Universitas gagal diperbaharui."),
                                        HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(univ, HttpStatus.OK);
    }
}
