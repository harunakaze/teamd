package id.ac.ui.cs.eaap.kelompokD.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by harunakaze on 12-May-17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StafModel {
    private String nip;
    private Integer id_universitas;
    private Double gaji_pokok;
    private String nama;
    private String tempat_lahir;
    private Date tgl_lahir;
    private String jenis_kelamin;
    private String agama;
    private String alamat;
    private Date tgl_perekrutan;
    private boolean IS_DELETED;
}
