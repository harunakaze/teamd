package id.ac.ui.cs.eaap.kelompokD.dao;

import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;
import id.ac.ui.cs.eaap.kelompokD.model.UniversitasModel;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by harunakaze on 27-May-17.
 */
@Mapper
public interface UniversitasMapper {

    @Insert("INSERT INTO universitas (nama_universitas, kepala_universitas) VALUES " +
            "(#{nama_universitas}, #{kepala_universitas})")
    @Options(useGeneratedKeys = true, keyProperty = "id_universitas")
    int newUniversitas(UniversitasModel newUniv);

    @Update("UPDATE universitas SET nama_universitas = #{nama_universitas}, kepala_universitas = #{kepala_universitas} " +
            "WHERE id_universitas = #{id_universitas}")
    int updUniversitas(UniversitasModel univ);

    @Select("SELECT * FROM universitas WHERE id_universitas = #{id_universitas}")
    UniversitasModel getUniversitas(@Param("id_universitas") Integer id_universitas);

    @Select("SELECT * FROM universitas")
    List<UniversitasModel> getAllUniversitas();

    @Select("SELECT * FROM staf_pembantu JOIN staf USING(nip, id_universitas) WHERE id_universitas = #{id_universitas}")
    List<StafPembantuModel> getAllStafPembantu(@Param("id_universitas") Integer id_universitas);
}
