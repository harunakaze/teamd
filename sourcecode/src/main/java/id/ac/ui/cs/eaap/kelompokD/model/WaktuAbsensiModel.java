package id.ac.ui.cs.eaap.kelompokD.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by harunakaze on 28-May-17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WaktuAbsensiModel {
    Date waktu_absensi;
    Date new_waktu_absensi;
}
