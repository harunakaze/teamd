package id.ac.ui.cs.eaap.kelompokD.rest;

import id.ac.ui.cs.eaap.kelompokD.model.*;
import id.ac.ui.cs.eaap.kelompokD.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by harunakaze on 27-May-17.
 */
@Slf4j
@RestController
public class PayrollRestController {

    @Autowired
    PayrollService payrollService;

    @Autowired
    StafPembantuService StafPembantuService;

    @Autowired
    StafDosenService stafDosenService;

    @RequestMapping("/rest/{id_universitas}/staf/gaji/{nip}/{year}/{month}")
    public PayrollModel hitungGajiStafPembantu(@PathVariable Integer id_universitas, @PathVariable String nip,
                                               @PathVariable Integer year, @PathVariable Integer month) {
        YearMonth yearMonth = getYearMonth(year, month);
        if(yearMonth == null) return null;

        StafPembantuModel staf = StafPembantuService.getPembantu(nip, id_universitas);

        return payrollService.getPayrollReportPembantu(staf, yearMonth);
    }

    @RequestMapping("/rest/{id_universitas}/dosen/gaji/{nidn}/{year}/{month}")
    public PayrollModel hitungGajiDosen(@PathVariable String nidn,
                                        @PathVariable Integer year, @PathVariable Integer month) {
        YearMonth yearMonth = getYearMonth(year, month);
        if(yearMonth == null) return null;

        StafDosenModel stafDosenModel = stafDosenService.getDosen(nidn);

        return payrollService.getPayrollReportDosen(stafDosenModel, yearMonth);
    }

    @RequestMapping("/rest/{id_universitas}/prodi/dosen/gaji/{id_prodi}/{year}/{month}")
    public List<PayrollModel> hitungDosenGajiPerProdi(@PathVariable Integer id_universitas, @PathVariable Integer id_prodi,
                                                 @PathVariable Integer year, @PathVariable Integer month) {
        YearMonth yearMonth = getYearMonth(year, month);
        if(yearMonth == null) return null;

        return payrollService.getDosenPayrollReportForProdi(id_prodi, yearMonth);
    }

    @RequestMapping("/rest/{id_universitas}/prodi/gaji/{id_prodi}/{year}/{month}")
    public List<PayrollModel> hitungGajiPerProdi(@PathVariable Integer id_universitas, @PathVariable Integer id_prodi,
                                                 @PathVariable Integer year, @PathVariable Integer month) {
        YearMonth yearMonth = getYearMonth(year, month);
        if(yearMonth == null) return null;

        return payrollService.getAllPayrollReportForProdi(id_prodi, yearMonth);
    }

    @RequestMapping("/rest/{id_universitas}/fakultas/gaji/{id_fakultas}/{year}/{month}")
    public List<PayrollModel> hitungGajiPerFakultas(@PathVariable Integer id_universitas, @PathVariable Integer id_fakultas,
                                                 @PathVariable Integer year, @PathVariable Integer month) {
        YearMonth yearMonth = getYearMonth(year, month);
        if(yearMonth == null) return null;

        return payrollService.getAllPayrollReportForFakultas(id_fakultas, yearMonth);
    }

    @RequestMapping("/rest/{id_universitas}/universitas/gaji/{year}/{month}")
    public List<PayrollModel> hitunGajiPerUniversitas(@PathVariable Integer id_universitas,
                                                    @PathVariable Integer year, @PathVariable Integer month) {
        YearMonth yearMonth = getYearMonth(year, month);
        if(yearMonth == null) return null;

        return payrollService.getAllPayrollReportForUniversitas(id_universitas, yearMonth);
    }

    // TODO : Remove this dummy request
    @RequestMapping("/rest/dosen/kehadiran/{nidn}/{year}/{month}")
    public String dummyRequestKehadiranDosen() {
        return "{ \"jumlah_hadir\" : 2 }";
    }

    private YearMonth getYearMonth(Integer year, Integer month) {
        YearMonth yearMonth;

        try {
            yearMonth = YearMonth.of(year, month);
        } catch (Exception e) {
            return null;
        }

        return yearMonth;
    }
}
