package id.ac.ui.cs.eaap.kelompokD.service;

import id.ac.ui.cs.eaap.kelompokD.model.AbsensiModel;
import id.ac.ui.cs.eaap.kelompokD.model.PayrollModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafDosenModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;

import java.time.YearMonth;
import java.util.List;

/**
 * Created by harunakaze on 27-May-17.
 */
public interface PayrollService {
    PayrollModel getPayrollReportPembantu(StafPembantuModel staf, YearMonth yearMonth);
    PayrollModel getPayrollReportDosen(StafDosenModel stafDosen, YearMonth yearMonth);
    List<PayrollModel> getDosenPayrollReportForProdi(Integer id_prodi, YearMonth yearMonth);
    List<PayrollModel> getAllPayrollReportForProdi(Integer id_prodi, YearMonth yearMonth);
    List<PayrollModel> getAllPayrollReportForFakultas(Integer id_fakultas, YearMonth yearMonth);
    List<PayrollModel> getAllPayrollReportForUniversitas(Integer id_universitas, YearMonth yearMonth);
}
