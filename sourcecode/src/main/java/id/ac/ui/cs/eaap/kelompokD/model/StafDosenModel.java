package id.ac.ui.cs.eaap.kelompokD.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by harunakaze on 12-May-17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StafDosenModel extends StafModel {
    private String id_prodi;
    private String nidn;
    private String pendidikan_terakhir;
}
