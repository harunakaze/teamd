package id.ac.ui.cs.eaap.kelompokD.service;

import id.ac.ui.cs.eaap.kelompokD.model.JabatanModel;
import id.ac.ui.cs.eaap.kelompokD.model.ProdiModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;

import java.util.List;

/**
 * Created by harunakaze on 26-May-17.
 */
public interface ProdiService {

    boolean newProdi(ProdiModel newProdi);
    boolean updProdi(ProdiModel prodi);

    ProdiModel getProdi(Integer id_prodi);
    List<ProdiModel> getAllProdi(Integer id_fakultas);

    boolean isProdiExist(ProdiModel prodi);

    JabatanModel getKetuaProdi(Integer id_prodi);
    List<StafPembantuModel> getAllStafPembantu(Integer id_prodi);
}
