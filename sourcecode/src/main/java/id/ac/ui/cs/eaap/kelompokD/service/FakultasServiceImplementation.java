package id.ac.ui.cs.eaap.kelompokD.service;

import id.ac.ui.cs.eaap.kelompokD.dao.FakultasMapper;
import id.ac.ui.cs.eaap.kelompokD.model.FakultasModel;
import id.ac.ui.cs.eaap.kelompokD.model.JabatanModel;
import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harunakaze on 26-May-17.
 */
@Service
public class FakultasServiceImplementation implements FakultasService {

    @Autowired
    FakultasMapper fakultasDAO;

    @Autowired
    JabatanService jabatanService;

    @Autowired
    StafPembantuService stafPembantuService;

    @Override
    public boolean newFakultas(FakultasModel newFakultas) {
        return (fakultasDAO.newFakultas(newFakultas) != 0);
    }

    @Override
    public boolean updFakultas(FakultasModel fakultas) {
        return (fakultasDAO.updFakultas(fakultas) != 0);
    }

    @Override
    public FakultasModel getFakultas(Integer id_fakultas) {
        return fakultasDAO.getFakultas(id_fakultas);
    }

    @Override
    public List<FakultasModel> getAllFakultas(Integer id_universitas) {
        return fakultasDAO.getAllFakultas(id_universitas);
    }

    @Override
    public boolean isFakultasExist(FakultasModel fakultas) {
        return (getFakultas(fakultas.getId_fakultas()) != null);
    }

    @Override
    public JabatanModel getKetuaFakultas(Integer id_fakultas) {
        return fakultasDAO.getKetuaFakultas(id_fakultas);
    }

    @Override
    public List<StafPembantuModel> getAllStafPembantu(Integer id_fakultas) {
        JabatanModel ketuaFakultas = getKetuaFakultas(id_fakultas);
        Integer kode_jabatan = ketuaFakultas.getKode_jabatan();

        JabatanModel jabatanModel = jabatanService.getStructureFromRoot(kode_jabatan);

        List<StafPembantuModel> listStaf = new ArrayList<>();
        stafPembantuService.getStafData(jabatanModel, listStaf);

        return listStaf;
    }
}
