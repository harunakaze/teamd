package id.ac.ui.cs.eaap.kelompokD.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by harunakaze on 27-May-17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class KehadiranDoseModel {
    private Integer jumlah_hadir;
}
