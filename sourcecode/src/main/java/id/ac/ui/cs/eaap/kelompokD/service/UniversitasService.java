package id.ac.ui.cs.eaap.kelompokD.service;

import id.ac.ui.cs.eaap.kelompokD.model.StafPembantuModel;
import id.ac.ui.cs.eaap.kelompokD.model.UniversitasModel;

import java.util.List;

/**
 * Created by harunakaze on 27-May-17.
 */
public interface UniversitasService {

    boolean newUniversitas(UniversitasModel newUniv);
    boolean updUniversitas(UniversitasModel univ);
    UniversitasModel getUniversitas(Integer id_universitas);

    boolean isUniversitasExist(UniversitasModel univ);

    List<UniversitasModel> getAllUniversitas();
    List<StafPembantuModel> getAllStafPembantu(Integer id_universitas);
}
