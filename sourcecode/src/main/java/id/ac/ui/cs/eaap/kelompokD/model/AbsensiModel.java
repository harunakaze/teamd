package id.ac.ui.cs.eaap.kelompokD.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Year;
import java.time.YearMonth;
import java.util.Date;
import java.util.List;

/**
 * Created by harunakaze on 27-May-17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AbsensiModel {
    private String nip;
    private Integer id_universitas;
    private List<Date> waktu_absen;
    private Integer jumlah_hadir;
}
